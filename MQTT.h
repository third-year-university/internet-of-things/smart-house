#include <PubSubClient.h>
#include <Servo.h> 
Servo s1;  

#define ON HIGH
#define OFF LOW
#define KETTLE_PIN D5

PubSubClient mqtt_cli(network_client);

String mqtt_broker = "m8.wqtt.ru";
int mqtt_port = 13394;
String mqtt_client_id = "esp8266" + id();
char* data;
String mqtt_login = "u_8WV5B2";
String mqtt_password = "RiKnuf8y";

String topics[] = {"lamp_left", "right_lamp", "back_lamp", "vent"};
String topics_states[] = {"lamp_left_state", "right_lamp_state", "back_lamp_state", "vent_state"};
int pins[] = {D2, D3, D1, D0};
int PINS = 4;

void callback(char* topic, byte* payload, unsigned int length){
  Serial.println("Message arrived on ");
  Serial.println(topic);


  for (int i = 0; i < PINS; i++){
    if (String(topic) == topics[i]){
      if (payload[0] == '0'){
        digitalWrite(pins[i], OFF);
        mqtt_cli.publish(topics_states[i].c_str(), String("0").c_str());
      }
      else{
        digitalWrite(pins[i], ON);
        mqtt_cli.publish(topics_states[i].c_str(), String("1").c_str());
      }
    }
  }
  if (String(topic) == "kettle"){
      if (payload[0] == '1'){
        s1.attach(KETTLE_PIN, 544, 2444);
        s1.write(90);
        delay(1000);
        s1.write(0);
        delay(1000);
        s1.detach();
        mqtt_cli.publish("kettle_state", String("0").c_str());
        mqtt_cli.publish("kettle", String("0").c_str(), true);
      }
    }
  for (int i = 0; i < length; i++){
    Serial.print((char)payload[i]);
  }
  Serial.println();
  Serial.println("---------------");
}


void init_devices(){
  for (int i = 0; i < PINS; i++){
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], OFF);
    mqtt_cli.publish(topics_states[i].c_str(), String('0' + "").c_str());
    mqtt_cli.subscribe(topics[i].c_str());
  }
  mqtt_cli.subscribe("kettle");
  mqtt_cli.publish("kettle_state", String("0").c_str());
  s1.attach(KETTLE_PIN, 544, 2444);
  s1.write(0);
  delay(3000);
  s1.detach();
}

void MQTT_init(){
  mqtt_cli.setServer(mqtt_broker.c_str(), mqtt_port);
  
  mqtt_cli.setCallback(callback);
  while(!mqtt_cli.connected()){
    Serial.print(mqtt_client_id);
    Serial.print(" connecting...");
    if (mqtt_cli.connect(mqtt_client_id.c_str(), mqtt_login.c_str(), mqtt_password.c_str())){
      Serial.println(" MQTT Connected!");
    }
    else{
      Serial.print("Failed to connect");
      Serial.println(mqtt_cli.state());
      delay(1000);
    }
  }
  init_devices();
}

void switch_off(){
  for (int i = 0; i < PINS; i++){
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], OFF);
    mqtt_cli.publish(topics_states[i].c_str(), String('0' + "").c_str());
    //mqtt_cli.subscribe(topics[i].c_str());
  }
  //mqtt_cli.subscribe("kettle");
  mqtt_cli.publish("kettle_state", String("0").c_str());
  s1.attach(KETTLE_PIN, 544, 2444);
  s1.write(0);
  delay(3000);
  s1.detach();
}
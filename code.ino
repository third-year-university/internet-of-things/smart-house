#include "WIFI.h"
#include "MQTT.h"
#include "ArduinoOTA.h"
#include <ESP8266WiFi.h>
#include "lib/ESP8266Ping.h"

#define LED_PIN D1

//ESP8266::IPAddress ip(192, 168, 0, 76);



void setup() {
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  WiFi_init(false);
  MQTT_init();
  ArduinoOTA.setPassword("super618");
  ArduinoOTA.begin();
  analogWriteResolution(16);
}

void loop() {
  //mqtt_cli.publish("termo", String(get_temperature()).c_str());
  mqtt_cli.loop();
  ArduinoOTA.handle();
  delay(10);

  if(!Ping.ping(IPAddress(192, 168, 0, 76))) {
    switch_off();
  }
}
